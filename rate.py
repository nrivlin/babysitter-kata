class Rate:

    def __init__(self, start_time, end_time, price_per_hour):
        if start_time < 5:
            self.start_time = start_time + 24
        else:
            self.start_time = start_time
        if end_time < 5:
            self.end_time = end_time + 24
        else:
            self.end_time = end_time
        self.price_per_hour = price_per_hour