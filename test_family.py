import rate
import family
import unittest

class TestFamily(unittest.TestCase):

    def test_family_attributes(self):
        rate_a1 = rate.Rate(start_time=17, end_time=23, price_per_hour=15)
        rate_a2 = rate.Rate(start_time=23, end_time=4, price_per_hour=20)
        family_a = family.Family(name='a', rate=[rate_a1, rate_a2])
        self.assertEqual('a', family_a.name)
        self.assertEqual([rate_a1, rate_a2], family_a.rate)
        self.assertEqual(17, family_a.rate[0].start_time)
        self.assertEqual(20, family_a.rate[1].price_per_hour)

if __name__ == '__main__':
    unittest.main()