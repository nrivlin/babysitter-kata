import rate
import unittest

class TestRate(unittest.TestCase):

    def test_rate_attributes(self):
        rate_a = rate.Rate(start_time=17, end_time=4, price_per_hour=15)
        self.assertEqual(17, rate_a.start_time)
        self.assertEqual(28, rate_a.end_time)
        self.assertEqual(15, rate_a.price_per_hour)

if __name__ == '__main__':
    unittest.main()