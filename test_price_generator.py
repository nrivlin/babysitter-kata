import rate
import family
import price_generator
import unittest

class TestPriceGenerator(unittest.TestCase):

    def test_price_generator_attributes(self):
        generator = price_generator.PriceGenerator(start_shift=19, end_shift=1, family='family_a')
        self.assertEqual(19, generator.start_shift)
        self.assertEqual(25, generator.end_shift)
        self.assertEqual('family_a', generator.family)

    def test_exceptions(self):
        generator = price_generator.PriceGenerator(start_shift=14, end_shift=1, family='family_a')
        self.assertEqual("The babysitter cannot start this early. Please select another time!",
                         generator.exceptions())
        generator = price_generator.PriceGenerator(start_shift=21, end_shift=5, family='family_a')
        self.assertEqual("The babysitter cannot stay this late. Please select another time!",
                         generator.exceptions())
        generator = price_generator.PriceGenerator(start_shift=21, end_shift=19, family='family_a')
        self.assertEqual("The end of the shift cannot be earlier than the start of the shift. Please select another time!",
                         generator.exceptions())

    def test_calcualate(self):
        rate_a1 = rate.Rate(start_time=17, end_time=23, price_per_hour=15)
        rate_a2 = rate.Rate(start_time=23, end_time=4, price_per_hour=20)
        family_a = family.Family(name='a', rate=[rate_a1, rate_a2])
        generator = price_generator.PriceGenerator(start_shift=19, end_shift=23, family=family_a)
        self.assertEqual(60, generator.calcualate())
        generator = price_generator.PriceGenerator(start_shift=19, end_shift=25, family=family_a)
        self.assertEqual(100, generator.calcualate())

    def test_calcualte_exceptions(self):
        generator = price_generator.PriceGenerator(start_shift=14, end_shift=1, family='family_a')
        self.assertEqual("The babysitter cannot start this early. Please select another time!",
                         generator.calcualate())
        generator = price_generator.PriceGenerator(start_shift=21, end_shift=5, family='family_a')
        self.assertEqual("The babysitter cannot stay this late. Please select another time!",
                         generator.calcualate())
        generator = price_generator.PriceGenerator(start_shift=21, end_shift=19, family='family_a')
        self.assertEqual("The end of the shift cannot be earlier than the start of the shift. Please select another time!",
                         generator.calcualate())

if __name__ == '__main__':
    unittest.main()