# BABYSITTER KATA
___

This project provides a calculator that generates a price per babysitting shift depending on the family's rate and the hour's rate. (i.e one family could be charged $15 between 5pm-10pm and $20 for the rest of the night, while a second family be charged $12 between 5pm and 9pm and $16 for the rest of the night)

# REQUIREMENTS
___

`Python2.7 or Greater `

There are no external dependencies outside of the Python standard library

# TO RUN THE PROJECT

___

1. GitLab Repository | [Link](https://gitlab.com/nrivlin/babysitter-kata) 
2. In command line:

`$ git clone https://gitlab.com/nrivlin/babysitter-kata.git`

`$ python babysitter-kata/run.py`


## CODE FUNCTIONALITY:

**Create a new rate:**
 
`rate_a1 = rate.Rate(start_time=17, end_time=23, price_per_hour=15)`

**Create a new family/client:**

`family_a = family.Family(name='a', rate=[rate_a1, rate_a2])`

**Generate a shift:**

`generator = price_generator.PriceGenerator(start_shift=19, end_shift=1, family=family_a)`

**Calculate price for shift:**

`generator.calcualate()`

**NOTE**

This is the order of operations that must be performed in order to generate a price.



# TO RUN THE TESTS

___

## RATE TEST:

`$ python pencil-durability/test_rate.py`

## FAMILY TEST:

`$ python pencil-durability/test_family.py`

## PRICE_GENERATOR TEST:

`$ python pencil-durability/test_price_generator.py`