import math

class PriceGenerator:

    def __init__(self, start_shift, end_shift, family):
        if start_shift < 5:
            self.start_shift = start_shift + 24
        else:
            self.start_shift = start_shift
        if end_shift < 5:
            self.end_shift = end_shift + 24
        else:
            self.end_shift = end_shift
        self.family = family

    def exceptions(self):
        if self.start_shift < 17:
            return "The babysitter cannot start this early. Please select another time!"
        elif self.end_shift < 17 or self.end_shift > 28:
            return ("The babysitter cannot stay this late. Please select another time!")
        elif self.end_shift < self.start_shift:
            return "The end of the shift cannot be earlier than the start of the shift." \
                   " Please select another time!"

    def calcualate(self):
        error = self.exceptions()
        if error is not None:
            print(error)
            return error
        else:
            price = 0
            for rate in self.family.rate[::-1]:
                if self.end_shift > rate.start_time and self.end_shift <= rate.end_time and self.start_shift < rate.start_time:
                    price = math.ceil(self.end_shift - rate.start_time) * rate.price_per_hour
                elif self.end_shift >= rate.end_time and self.start_shift <= rate.start_time:
                    price += (rate.end_time - rate.start_time) * rate.price_per_hour
                elif self.start_shift < rate.end_time and self.end_shift > rate.end_time:
                    price += math.ceil(rate.end_time - self.start_shift) * rate.price_per_hour
                elif self.start_shift >= rate.start_time and self.end_shift <= rate.end_time:
                    price += math.ceil(self.end_shift-self.start_shift) * rate.price_per_hour
            print(price)
            return price