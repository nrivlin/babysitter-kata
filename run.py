import rate
import family
import price_generator

def run():
    rate_a1 = rate.Rate(start_time=17, end_time=23, price_per_hour=15)
    rate_a2 = rate.Rate(start_time=23, end_time=4, price_per_hour=20)
    rate_b1 = rate.Rate(start_time=17, end_time=22, price_per_hour=12)
    rate_b2 = rate.Rate(start_time=22, end_time=24, price_per_hour=8)
    rate_b3 = rate.Rate(start_time=24, end_time=4, price_per_hour=16)
    rate_c1 = rate.Rate(start_time=17, end_time=21, price_per_hour=21)
    rate_c2 = rate.Rate(start_time=21, end_time=4, price_per_hour=15)
    family_a = family.Family(name='a', rate=[rate_a1, rate_a2])
    family_b = family.Family(name='b', rate=[rate_b1, rate_b2, rate_b3])
    family_c = family.Family(name='c', rate=[rate_c1, rate_c2])
    generator = price_generator.PriceGenerator(start_shift=19, end_shift=1, family=family_a)
    generator.calcualate()
    generator = price_generator.PriceGenerator(start_shift=18, end_shift=3, family=family_b)
    generator.calcualate()
    generator = price_generator.PriceGenerator(start_shift=17, end_shift=24, family=family_c)
    generator.calcualate()
    generator = price_generator.PriceGenerator(start_shift=15, end_shift=22, family=family_a)
    generator.calcualate()
    generator = price_generator.PriceGenerator(start_shift=19, end_shift=29, family=family_b)
    generator.calcualate()
    generator = price_generator.PriceGenerator(start_shift=19, end_shift=18, family=family_c)
    generator.calcualate()

if __name__ == "__main__":
    run()